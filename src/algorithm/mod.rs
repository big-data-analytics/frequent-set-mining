pub mod a_priori;

pub const MAX_AMT_EXAMPLES_TO_SHOW: usize = 8;
const START_SIZE_AUTHORS: usize = 2048;
pub const DEFAULT_SUPPORT_THRESHOLD: u32 = 512;

pub type AuthorIndex = u32;
type FrequentPrevPassItemSets<Item> = Vec<Vec<Item>>;

/// Trait used to implement a frequent set search algorithm.
pub trait FrequentSetAlgorithm<'a>
where Self: Sized
{
    type Item: std::fmt::Debug + std::hash::Hash + Eq + Clone;

    fn nth_pass(&self) -> u8;

    fn support_threshold(&self) -> u32;

    /// The frequent item sets from the previous pass, along with their count.
    fn frequent_prev_pass_item_sets(&'a self) -> &'a FrequentPrevPassItemSets<Self::Item>;

    /// Performs a pass of a implemented algorithm. The new frequent item sets,
    /// along with their count, should be stored in the struct.
    /// The size of the item sets should equal the nth pass which is being
    /// performed.
    fn pass(self) -> Result<Self, String>;
}
