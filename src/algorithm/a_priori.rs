use super::AuthorIndex;
use crate::{
    algorithm::*,
    db_reader::{AuthorName, BooksReader, DEFAULT_DB_PATH},
    help_fn::log_max_freq_and_examples,
    item_index_map::ItemIndexMap,
};
use itertools::Itertools;
use std::{
    collections::{HashMap, HashSet},
    hash::Hash,
};

/// The intermediate result of a number A-Priori passes.
pub struct APriori {
    /// Counter for the amount of passes already performed.
    pass: u8,
    /// Support threshold to filter frequent items.
    support_threshold: u32,
    /// Frequent multiple-item sets from the previous pass.
    frequent_prev_pass_item_sets: FrequentPrevPassItemSets<AuthorIndex>,
    /// Items that are still frequent somewhere. Ones that aren't in this
    /// set, will be eliminated.
    frequent_items: HashSet<AuthorIndex>,
    /// Mapping from a string to idx
    item_idx_map: ItemIndexMap<AuthorName, AuthorIndex>,
    /// Path to the database.
    db_path: String,
    /// Whether we should print results between runs.
    verbose: bool,
}

impl APriori {
    pub fn new(support_threshold: u32, db_path: String, verbose: bool) -> Self {
        Self {
            pass: 0,
            support_threshold,
            frequent_prev_pass_item_sets: Vec::new(),
            frequent_items: HashSet::default(),
            item_idx_map: ItemIndexMap::with_capacity(START_SIZE_AUTHORS),
            db_path,
            verbose,
        }
    }
}

impl Default for APriori {
    fn default() -> Self { Self::new(DEFAULT_SUPPORT_THRESHOLD, DEFAULT_DB_PATH.to_string(), true) }
}

/// Helper functions
impl APriori {
    /// Checks whether the given set is frequent given the last pass' frequent
    /// sets.
    fn is_frequent<Item: PartialEq + Clone + Hash + Eq>(
        set_from_basket: Vec<Item>,
        last_pass_freq_sets: &FrequentPrevPassItemSets<Item>,
    ) -> bool
    {
        let freq_subset_len = set_from_basket.len() - 1;

        set_from_basket
            .into_iter()
            .combinations(freq_subset_len)
            .all(|subset| last_pass_freq_sets.contains(&subset))
    }
}

impl<'a> FrequentSetAlgorithm<'a> for APriori {
    type Item = AuthorIndex;

    fn nth_pass(&self) -> u8 { self.pass }

    fn support_threshold(&self) -> u32 { self.support_threshold }

    fn frequent_prev_pass_item_sets(&'a self) -> &'a FrequentPrevPassItemSets<Self::Item> {
        &self.frequent_prev_pass_item_sets
    }

    /// Perform a pass of the algorithm.
    /// Note: Rust's [HashMap](https://doc.rust-lang.org/std/collections/struct.HashMap.html)
    /// implementation uses [quadratic probing](https://en.wikipedia.org/wiki/Quadratic_probing).
    /// This ensures that, even when the key's hashes collide, each key will
    /// have a unique value associated with it.
    fn pass(self) -> Result<Self, String> {
        let nth_pass = self.pass;
        let last_pass_freq_sets = self.frequent_prev_pass_item_sets;
        let support_threshold = self.support_threshold;
        let mut item_idx_map = self.item_idx_map;
        let freq_items = self.frequent_items;

        let baskets = BooksReader::from_path(&self.db_path);
        let sets_with_count = baskets?
            .inspect(|basket| {
                // log errors during parsing of the basket
                if let Err(e) = basket {
                    println!("{}", e);
                }
            })
            .filter_map(Result::ok)
            .map(|parsed_basket| {
                // convert items to indices
                parsed_basket
                    .into_iter()
                    .map(|item| item_idx_map.value_or_insert(item))
                    .collect_vec()
            })
            .fold(
                HashMap::<Vec<Self::Item>, usize>::with_capacity(START_SIZE_AUTHORS / 4),
                |mut occurrences, mut basket| {
                    // Count every item if this is the first pass
                    if nth_pass == 0 {
                        for item in basket {
                            let counter = occurrences.entry(vec![item]).or_insert(0);
                            *counter += 1;
                        }
                    } else {
                        // We do not generate all possible frequent sets and check each one of them
                        // for occurrence in the basket. This approach would make us generate a lot
                        // of sets which do not even exist in the database.
                        // Instead we generate all sets from a book and check whether they are
                        // frequent.
                        //
                        basket.sort();
                        basket
                            .into_iter()
                            .filter(|item| freq_items.contains(item))
                            .combinations(nth_pass as usize + 1)
                            .filter(|set_from_basket| {
                                // NOTE: this filter could be removed. If done
                                // so the memory usage
                                // increases and runtime decreases. It's a
                                // trade-off.
                                Self::is_frequent(set_from_basket.clone(), &last_pass_freq_sets)
                            })
                            .for_each(|freq_set_from_basket| {
                                let counter = occurrences.entry(freq_set_from_basket).or_insert(0);
                                *counter += 1;
                            })
                    }

                    occurrences
                },
            )
            .into_iter();

        // filter the frequent sets (support >= support threshold)
        // NOTE: we need duplicates of this iterator as they are consumed on use (i.e.
        // passing one by reference doesn't work). Usually this is solved with
        // ``cloned()``. However, the iterator comes from a HashMap and the items are
        // (&K, &V) and ``cloned()`` expects a ``&_``.
        let freq_sets_with_count = sets_with_count
            .filter(|&(_, occurrence)| occurrence >= support_threshold as usize)
            .collect::<Vec<(Vec<Self::Item>, usize)>>();

        // log frequent sets with count [side-effects in my functional language >:( ]
        let this_pass = nth_pass + 1;
        if self.verbose {
            log_max_freq_and_examples(&freq_sets_with_count, &item_idx_map, this_pass);
        }

        let new_freq_sets: Vec<Vec<u32>> = freq_sets_with_count
            .into_iter()
            .map(|(set, _)| set)
            .collect();

        let frequent_items: HashSet<u32> =
            new_freq_sets
                .iter()
                .fold(HashSet::<u32>::new(), |mut map, set| {
                    set.iter().fold(&mut map, |map, item| {
                        map.insert(*item);
                        map
                    });
                    map
                });

        Ok(Self {
            pass: this_pass,
            support_threshold,
            // store only frequent sets for the next pass
            frequent_prev_pass_item_sets: new_freq_sets,
            frequent_items,
            item_idx_map,
            db_path: self.db_path,
            verbose: self.verbose,
        })
    }
}
