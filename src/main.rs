mod algorithm;
mod db_reader;
mod help_fn;
mod item_index_map;

use crate::help_fn::*;
use algorithm::FrequentSetAlgorithm;

fn main() -> Result<(), String> {
    let args = parse_cli_args();
    let passes: u8 = args
        .value_of("passes")
        .and_then(|val| val.parse().ok())
        .unwrap_or(std::u8::MAX);

    let file = args.value_of("file").unwrap();
    let t = args.value_of("threshold").unwrap();
    let mut a_priori = algorithm::a_priori::APriori::new(
        t.parse().expect("Invalid threshold"),
        String::from(file),
        true,
    );
    for pass_ctr in 0..passes {
        assert_eq!(a_priori.nth_pass(), pass_ctr);
        a_priori = a_priori.pass()?;
        if a_priori.frequent_prev_pass_item_sets().is_empty() {
            break
        }
    }

    Ok(())
}
