use crate::{
    algorithm::{AuthorIndex, MAX_AMT_EXAMPLES_TO_SHOW},
    db_reader::AuthorName,
    item_index_map::ItemIndexMap,
};
use clap::{App, Arg};
use itertools::Itertools;

/// Outputs the actual information asked in the assignment.
pub fn log_max_freq_and_examples(
    sets: &[(Vec<AuthorIndex>, usize)],
    author_index_map: &ItemIndexMap<AuthorName, AuthorIndex>,
    nth_pass: u8,
)
{
    if sets.is_empty() {
        println!("Pass nr. {} is empty", nth_pass);
    } else {
        // get the frequency of the most frequent set
        let max_freq = sets
            .iter()
            .map(|(_, count)| count)
            .max()
            .expect("Failed to calculate maximum frequency for previous pass' item sets.");

        let avg_freq = sets.iter().map(|(_, count)| count).sum::<usize>() / sets.len();

        println!(
            "PASS {}: {} frequent sets, {} max in one set, {} avg. Some frequent example sets are:",
            nth_pass,
            sets.len(),
            max_freq,
            avg_freq,
        );
        // get a list of example sets close to the max frequency and format them
        for (authors, count) in sets.iter().take(MAX_AMT_EXAMPLES_TO_SHOW) {
            let set_list = authors
                .iter()
                .filter_map(|author_index| author_index_map.reverse_lookup(*author_index))
                .collect_vec();
            println!("- {:?}: {}", set_list, count);
        }
    }
}

pub fn parse_cli_args() -> clap::ArgMatches<'static> {
    App::new("Frequent Authors")
        .arg(
            Arg::with_name("passes")
                .help(
                    "Specifies the requested amount of passes. Defaults to infinity (keep going \
                     until the pass is empty)",
                )
                .short("k")
                .takes_value(true)
                .required(false),
        )
        .arg(
            Arg::with_name("threshold")
                .help("Specifies the support threshold to use")
                .required(false)
                .default_value("512")
                .short("t"),
        )
        .arg(
            Arg::with_name("file")
                .help("Specifies the XML-file to use")
                .required(false)
                .default_value("dblp.xml")
                .short("f"),
        )
        .get_matches()
}
