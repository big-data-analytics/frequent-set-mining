use std::collections::HashMap;

pub struct ItemIndexMap<
    Item: std::hash::Hash + Eq + AsRef<[u8]>,
    IndexType: Default + Copy + std::ops::AddAssign<u32> + PartialEq,
> {
    cur_index: IndexType,
    map:       HashMap<Item, IndexType>,
}

impl<
        Item: std::hash::Hash + Eq + AsRef<[u8]>,
        IndexType: Default + Copy + std::ops::AddAssign<u32> + PartialEq,
    > ItemIndexMap<Item, IndexType>
{
    pub fn with_capacity(capacity: usize) -> Self {
        Self {
            cur_index: IndexType::default(),
            map:       HashMap::<Item, IndexType>::with_capacity(capacity),
        }
    }

    pub fn value_or_insert(&mut self, key: Item) -> IndexType {
        let index = self.map.entry(key).or_insert(self.cur_index);
        self.cur_index += 1;
        *index
    }

    // Note: expensive! Only used during printing of results.
    pub fn reverse_lookup(&self, value: IndexType) -> Option<String> {
        self.map
            .iter()
            .filter(|(_, map_value)| value == **map_value)
            .map(|(name, _)| String::from_utf8_lossy(name.as_ref()).into_owned())
            .next()
            .map(|s| s.clone())
    }
}
