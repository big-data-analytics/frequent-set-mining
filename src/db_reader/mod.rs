use std::{
    fs::File,
    io::{BufRead, BufReader},
    path::Path,
};

use quick_xml::Reader;

pub const DEFAULT_DB_PATH: &str = "dblp.xml";

pub type AuthorName = Vec<u8>;
pub type Book = Vec<AuthorName>;

pub struct BooksReader<R: BufRead> {
    xml_file_reader: Reader<R>,
    bufr:            Vec<u8>,
}

impl<R: BufRead> Iterator for BooksReader<R> {
    /// Type used to read towards
    type Item = Result<Book, String>;

    /// Returns the next book, or "basket" in freq set terms.
    fn next(&mut self) -> Option<Self::Item> {
        use quick_xml::events::Event::*;
        loop {
            match self.xml_file_reader.read_event(&mut self.bufr) {
                Ok(Start(ref tag)) => {
                    match tag.name() {
                        b"article" | b"inproceedings" | b"proceedings" | b"book"
                        | b"incollection" | b"phdthesis" | b"mastersthesis" | b"www" => {
                            break Some(parse_book(&mut self.xml_file_reader, &mut self.bufr))
                        }
                        err_tag => {
                            break Some(Err(format!(
                                "Unexpected opening tag: {}",
                                String::from_utf8_lossy(err_tag)
                            )))
                        }
                    }
                }
                Ok(Eof) | Ok(End(_)) => break None,
                // These should not be called
                Ok(DocType(ref tag)) | Ok(Text(ref tag)) | Ok(PI(ref tag)) => {
                    break Some(Err(format!(
                        "Unexpected event: {}",
                        String::from_utf8_lossy(tag)
                    )))
                }
                Ok(Empty(ref tag)) => {
                    break Some(Err(format!(
                        "Unexpected event: {}",
                        String::from_utf8_lossy(tag)
                    )))
                }
                Ok(Decl(_)) | Ok(CData(_)) | Ok(Comment(_)) => continue,
                Err(err) => break Some(Err(format!("Error when reading XML: {:?}", err))),
            }
        }
    }
}

impl<R: BufRead> BooksReader<R> {
    pub fn from_read(read: R) -> Result<Self, String> {
        use quick_xml::events::Event::*;
        let mut reader = Reader::from_reader(read);
        reader.trim_text(true);
        reader.check_comments(false);
        reader.trim_markup_names_in_closing_tags(true);

        // Open the document
        let mut buf = Vec::new();

        // Open the root element
        loop {
            if let Ok(Start(ref tag)) = reader.read_event(&mut buf) {
                if tag.name() == b"dblp" {
                    break
                }
            }
        }

        Ok(BooksReader {
            xml_file_reader: reader,
            bufr:            buf,
        })
    }
}

impl BooksReader<BufReader<std::fs::File>> {
    /// Opens database from the given path
    pub fn from_path<P: AsRef<Path>>(path: P) -> Result<Self, String> {
        let file = File::open(&path).map_err(|e| {
            format!(
                "Failed to open file from path {}: {}",
                path.as_ref().display(),
                e
            )
        })?;
        let bufread = BufReader::new(file);

        Self::from_read(bufread)
    }
}

fn parse_book<R: BufRead>(parser: &mut Reader<R>, buf: &mut Vec<u8>) -> Result<Book, String> {
    use quick_xml::events::Event::*;
    let mut book = Book::new();

    loop {
        match parser.read_event(buf) {
            Ok(Start(ref tag)) => {
                match tag.name() {
                    b"author" => {
                        // Insert the author in the alphabetical order
                        let author = parse_author(parser, buf)?;
                        let idx = book.binary_search(&author).unwrap_or_else(|x| x);

                        book.insert(idx, author);
                    }
                    b"title" | b"booktitle" | b"pages" | b"year" | b"journal" | b"volume"
                    | b"number" | b"month" | b"url" | b"ee" | b"cite" | b"school"
                    | b"publisher" | b"note" | b"cdrom" | b"crossref" | b"isbn" | b"chapter"
                    | b"series" | b"publnr" | b"editor" | b"address" => {
                        // As these are not required for any assignment so far, these values are not
                        // kept.
                        if let Err(err) = parse_unimportant(parser, buf) {
                            return Err(err)
                        }
                    }
                    tagname => {
                        return Err(format!(
                            "Unexpected opening tag: {}",
                            String::from_utf8_lossy(tagname)
                        ))
                    }
                }
            }
            Ok(End(_)) => {
                book.shrink_to_fit();
                buf.clear();
                return Ok(book)
            }
            Err(err) => return Err(format!("Error reading book: {}", err)),
            Ok(event) => return Err(format!("Unexpected while reading book: {:?}", event)),
        }
    }
}

/// Parses the author of a book. Returns the indmutex.
fn parse_author<R: BufRead>(
    parser: &mut Reader<R>,
    buf: &mut Vec<u8>,
) -> Result<AuthorName, String>
{
    use quick_xml::events::Event::*;

    // Read the name
    let author_name = match parser.read_event(buf) {
        Ok(Text(name)) => name.into_owned().to_vec(),
        Ok(event) => return Err(format!("Expected name of author, got {:?}", event)),
        Err(err) => return Err(format!("Expected name of author, got error {}", err)),
    };

    // Read the closing tag
    match parser.read_event(buf) {
        Ok(End(_)) => {}
        Ok(event) => return Err(format!("Expected end of author, got {:?}", event)),
        Err(err) => return Err(format!("Expected end of author, got error {}", err)),
    }

    Ok(author_name)
}

/// Parses data that should not be kept. Returns nothing
fn parse_unimportant<R: BufRead>(parser: &mut Reader<R>, buf: &mut Vec<u8>) -> Result<(), String> {
    use quick_xml::events::Event::*;
    let mut depth = 1;
    while depth > 0 {
        let parse = parser.read_event(buf);
        match parse {
            Ok(Text(_)) => {}
            Ok(Start(_)) => depth += 1,
            Ok(End(_)) => depth -= 1,
            Ok(Eof) => {
                return Err("Unexpected end of file in parsing unimportant things".to_string())
            }
            Ok(_) => {
                return Err("Error while parsing unimportant things: unexpected stuff".to_string())
            }
            Err(err) => return Err(format!("Error parsing: {}", err)),
        }
    }
    Ok(())
}

#[test]
fn read_small_db() {
    let db_iter = BooksReader::from_path("./testfiles/small.xml");
    assert_eq!(db_iter.unwrap().count(), 3);
}

#[test]
fn read_medium_db() {
    let db_iter = BooksReader::from_path("./testfiles/medium.xml");
    assert_eq!(db_iter.unwrap().count(), 60);
}
